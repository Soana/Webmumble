var Audio = require('../audio');
var Util = require('util');
var EventEmitter = require('events').EventEmitter;
var Command = require('./command');

var Bumble = function(obj) {
	this.websocketUrl = obj.websocketUrl;
	this.audio = new Audio({
		sampleRate : obj.sampleRate,
		bufferSize : obj.bufferSize
	});
	this.audio.on('error', this._onError.bind(this));
	this.command = new Command();
	this.on('newListener', function (event, listener) {
		this.command.on(event, listener);
	});
};

Util.inherits(Bumble, EventEmitter);

Bumble.prototype.onServerJoined = function(success) {
	if(success){
		this.emit('server-joined');
	}
	else{
		this._onError(new Error("Could not join server"));
	}
};

Bumble.prototype.start = function() {
	this._acquireAudio();
};

Bumble.prototype._setupWebsocket = function() {
	this.socket = new BinaryClient(this.websocketUrl);
	this.socket.binaryType = "arraybuffer";
	this.socket.on('open', this._websocketOpened.bind(this));
	this.socket.on('stream', this._onStream.bind(this))
};

Bumble.prototype._onStream = function(stream) {
	stream.on('data', function(data) {
		if(data.type === "input") {
			this.audio.setInputStream(stream);
		}
		else if(data.type === "output") {
			this.audio.addOutputStream(data.session, stream);
		}
		else if(data.type === "command") {
			this.command.setStream(stream);
			this.emit('ready');
		}
	}.bind(this));
};

/**
 * Local mutes the user. Cannot mute the current User. Use {@link Bumble.muteSelf()} instead-
 * @param {User} user - the user to mute
 * @param {boolean} muted - whether the user should be local muted; if undefined, the local mute-state will be toggled
 * @returns {boolean} the new local mute-state of the user
 */
Bumble.prototype.localMuteUser = function(user, muted){
	if(muted === undefined){
		muted = ! this.audio.outputs[user.session].muted;
	}
	this.audio.outputs[user.session].mute(muted);
	user.localMute = muted;
	return muted;
};

/**
 * Mutes the currently authenticated user.
 * @param {User} user - the currently authenticated user
 * @param {boolean} muted - whether the user should be muted; if undefined, the mute-state will be toggled
 * @returns {boolean} the new mute-state of the current user
 */
Bumble.prototype.muteSelf = function(user, muted){
	if(muted === undefined){
		muted = ! this.audio.input.muted;
	}
	this.audio.input.mute(muted);
	user.selfMute = muted;
	return muted;
}

Bumble.prototype.joinChannel = function(channel) {
	this.command.joinChannel(channel);
};

Bumble.prototype.joinServer = function(server, port, username, password) {
	this.command.joinServer(server, port, username, password, this.onServerJoined.bind(this));
	this.command.on("authenticated", function(userSessionID){
		this.currentSession = userSessionID;
	}.bind(this));
};

Bumble.prototype._acquireAudio = function() {
	navigator.getUserMedia({ audio: true }, this._audioAcquired.bind(this),  this._onError.bind(this));
};

Bumble.prototype._setupAudio = function() {
	this.audio.setupAudio(this.context, this.source, this.destination);
	this._setupWebsocket();
};

Bumble.prototype._audioAcquired = function(stream) {
	this.emit('audio-acquired');
	this.context = new AudioContext();
	this.source = this.context.createMediaStreamSource(stream);
	this.destination = this.context.destination;
	this._setupAudio();
};

Bumble.prototype._websocketOpened = function(stream) {
	this.emit('connected');
};

Bumble.prototype._onError = function(err) {
	this.emit('error', err);
};

module.exports = Bumble;
