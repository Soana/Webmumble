var React = require("react");

var User = React.createClass({
	render: function() {
		var deaf = this.props.user.selfDeaf || this.props.user.deaf;
		return (
			<li className="user">
				<i className="icon fa fa-user"></i>
				{this.props.user.name}
				<i className="trailing-icon local-mute fa fa-microphone-slash" style={{display: this.props.user.localMute?"inline-block":"none"}}></i>
				<i className="trailing-icon self-mute fa fa-microphone-slash" style={{display: this.props.user.selfMute?"inline-block":"none"}}></i>
				<i className="trailing-icon global-mute fa fa-microphone-slash" style={{display: this.props.user.mute?"inline-block":"none"}}></i>
				<i className="trailing-icon suppressed fa fa-microphone-slash" style={{display: this.props.user.suppress?"inline-block":"none"}}></i>
				<i className="trailing-icon fa fa-volume-off" style={{display: this.props.user.suppress?"inline-block":"none"}}></i>
				<div className="dropdown">
					<button className="btn btn-xs btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<i className="icon fa fa-toggle-down"></i>
					</button>
					<ul className="dropdown-menu" aria-labelledby="dropdownMenu1">
						<li onClick={this.mute}><span className="text-nowrap"><i className="icon fa fa-check" style={{visibility: this.props.user.localMute?"visible":"hidden"}}></i> Local Mute</span></li>
					</ul>
				</div>
			</li>
		);
	},

	mute: function(){
		// console.log(this.props.user);
		// var muteCheck = $("#user" + this.props.user.session + "-mute-check");
		// if(muteCheck.css("visibility") === "visible"){
		// 	muteCheck.css("visibility", "hidden");
		// }
		// else{
		// 	muteCheck.css("visibility", "visible");
		// }
		this.props.onEvent.mute(this.props.user);
	}
});

module.exports = User;
