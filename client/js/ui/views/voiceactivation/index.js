var React = require("react");
var ReactSlider = require("react-slider");

var VoiceActivation = React.createClass({

	componentWillReceiveProps: function(props){
		this.setState({
			low: props.low,
			high: props.high
		});
	},

	getInitialState: function(){
		return {
			low: 50,
			high: 75
		};
	},

	getDefaultProps: function(){
		return {
			low: 50,
			high: 75
		};
	},

	handleChange: function(values){
		this.setState({
			low: values[0],
			high: values[1]
		});
	},

	render: function() {
		//return <ReactSlider defaultValue={[this.props.defaultActivation.lower, this.props.defaultActivation.higher]} withBars />
		return (
			<ReactSlider value={[this.state.low, this.state.high]} withBars={true} onChange={this.handleChange}>
			</ReactSlider>
		);
	}
});

module.exports = VoiceActivation;
